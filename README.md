# Description du projet

XXXXXXXX XXXXXX

## Gitlab

[Gitlab projet](https://bitbucket.org/blinjm/)
Ajouter un projet dans la liste puis changer cette ligne avec le lien direct vers le projet

## Installation

Installer [gulp](https://gulpjs.com/docs/en/getting-started/quick-start) et [npm](https://nodejs.org/en/download/) au préalable

```bash
npm install --save-dev gulp
npm install
```

Lancement dans le navigateur

```bash
gulp
```

## Utilisation include

Tous les fichiers include doivent se placer dans le dossier MK_content > include.
Tous les appels doivent se faire dans des fichiers .htm, gulp créera un fichier .html compilé avec l'include au même niveau de l'arborescence.
