// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var babel = require("gulp-babel");
var browserSync = require('browser-sync').create();
var plumber = require('gulp-plumber');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var postcss = require('gulp-postcss');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
var cleanCSS = require('gulp-clean-css');
var notify = require('gulp-notify');
var fileinclude = require('gulp-file-include');

var onError = function(err) {
  notify.onError({
    title: "Gulp",
    subtitle: "Failure!",
    message: "Error: <%= error.message %>",
    sound: "Basso"
  })(err);
  this.emit('end');
};

// Include HTML
gulp.task('build-html' , function(){
    gulp.src(["./MK_content/*/*.htm", "./MK_content/*.htm"])
        .pipe(fileinclude({
          prefix: '@@',
          basepath: '@file'
        }))
        .pipe(rename(function (path) {
          path.extname = ".html";
        }))
        .pipe(gulp.dest('./MK_content/'));
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/*.js')
        .pipe(jshint({esversion: 6}))
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('scss/*.scss')
        .pipe(plumber({
          errorHandler: onError
        }))
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([ autoprefixer() ]))
        .pipe(concat('all.css'))
        .pipe(gulp.dest('dist'))
        .pipe(cleanCSS())
        .pipe(rename('all.min.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.stream());
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
  /*  return gulp.src(['js/vendor/*.js', 'js/*.js','node_modules/jquery-touchswipe/jquery.touchSwipe.min.js', 'js/react/*.js'])
        .pipe(plumber({
          errorHandler: onError
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
    			presets: ['@babel/preset-env']
    		}))
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/js'));*/
    return gulp.src(['js/vendor/*.js', 'js/*.js','node_modules/jquery-touchswipe/jquery.touchSwipe.min.js', 'js/react/*.js'])
      .pipe(sourcemaps.init())
      .pipe(babel({
        "presets": ["@babel/preset-react"]
      }))
      .pipe(concat('all.js'))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('dist'));
});

// create a task that ensures the `js` task is complete before
// reloading browsers
gulp.task('js-watch', ['scripts'], function (done) {
    browserSync.reload();
    done();
});

// Watch Files For Changes
gulp.task('watch', function() {

    //browserSync for HTML
    browserSync.init({
      files: ['*.htm', '*.html'],
      server: {
        baseDir: "./"
      },
      options: {
        reloadDelay: 250
      },
      notify: false
    });

    gulp.watch('js/*.js', ['lint', 'scripts']);
    gulp.watch('js/react/*.js', ['lint', 'scripts']);
    gulp.watch('scss/_module/*.scss', ['sass']);
    gulp.watch('scss/_common/*.scss', ['sass']);
    gulp.watch('scss/_module/*/*.scss', ['sass']);
    gulp.watch('MK_content/*/*.htm', ['build-html']);
    gulp.watch('MK_content/*.htm', ['build-html']);
    gulp.watch('MK_content/include/*.html', ['build-html']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch', 'build-html']);
