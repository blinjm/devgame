// METIER
class Metier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
  		nom: this.props.nom,
      education: this.props.edu,
      production: this.props.prod,
      argent: this.props.argent,
      physique: this.props.physique,
      activite_saison1: this.props.ac1,
      activite_saison2: this.props.ac2,
      activite_saison3: this.props.ac3,
      activite_saison4: this.props.ac4,
      debouche: this.props.debouche
    };
  }

  render() {
	  return(
      <div className="metier" data-id={this.state.nom}>
        <div className="education hidden">{this.state.education}</div>
        <div className="argent hidden">{this.state.argent}</div>
        <div className="production hidden">{this.state.production}</div>
        <div className="activite_saison1 hidden">{this.state.activite_saison1}</div>
        <div className="activite_saison2 hidden">{this.state.activite_saison2}</div>
        <div className="activite_saison3 hidden">{this.state.activite_saison3}</div>
        <div className="activite_saison4 hidden">{this.state.activite_saison4}</div>
        <div className="debouche hidden">{this.state.debouche}</div>
      </div>
	  );
  }
}
