// METIER
var default_revenu = 1;
var default_depense = 1;
var default_hab = "hutte";

class Famille extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
  		id: this.props.idFamille,
      membre: this.getMembreByProps(this.props.membre),
      revenu: (this.props.revenu != undefined)? parseInt(this.props.revenu):default_revenu,
      depense: (this.props.depense != undefined)? parseInt(this.props.depense):default_depense,
      saving: (this.props.saving != undefined)? parseInt(this.props.saving):this.getRandomFric(),
      habitation: (this.props.habitation != undefined)? (this.props.habitation):default_hab
    };
  }

  getRandomFric(){
    return Math.floor(Math.random() * 45);
  }
  getMembreByProps(membre){
    var array_membre = membre.toString().split(',');
    if(array_membre[0] == ""){
      array_membre = [];
    }
    return array_membre;
  }

  setVarAdd(field, myid){
    // var myid = this.state.id;
    var new_membre = parseInt($('.Famille[data-id="'+myid+'"] .ajoutmembre').html());
    if(new_membre != undefined){
      var membres_array = this.state.membre;
      membres_array.push(""+new_membre);
      $('.Famille[data-id="'+myid+'"] .membreFamille').html(membres_array.join(','));
      this.setState({
        membre: membres_array
      });
      $('.ajoutmembre').html('');
      this.ajustementRevenus();
    }
  }
  setVarSup(field, myid){
    var old_membre = parseInt($('.Famille[data-id="'+myid+'"] .suppmembre').html());
    if(old_membre != undefined){
      var membres_array = this.state.membre;
      var index = membres_array.indexOf(""+old_membre);
      if (index > -1) {
        membres_array.splice(index, 1);
      }
      
      if(membres_array.length == 0){
        this.setState({
          membre: membres_array,
          revenu: 0,
          depense: 0,
          saving: 0,
          habitation: ""
        });
      } else {
        this.setState({
          membre: membres_array
        });
      }
      $('.suppmembre').html('');
      this.ajustementRevenus();
    }
  }

  setVarHab(field, id){
    var newhome = $('.Famille[data-id="'+id+'"] .changeHabitation').html();
    var newdepense = $('.habitation[data-nom="'+newhome+'"] .loc').html();
    this.setState({
      'habitation': newhome,
      'depense': newdepense
    });
    debug_jmb('Famille '+id+' change de maison :'+newhome, 'brown');
  }

  // AJOUT DES REVENUS EN FIN DE MOIS
  getRevenu(myid){
    var temp_rev = this.state.revenu;
    var temp_dep = this.state.depense;
    var temp_sav = this.state.saving;
    if(temp_sav + temp_rev - temp_dep < 0){
      console.log("ARGENT 0: "+myid);
    }
    this.setState({
      saving: temp_sav + temp_rev - temp_dep
    });
  }

  // AJUSTEMENT DES REVENUS EN FONCTIONS DES MEMBRES DE LA FAMILLE
  ajustementRevenus(){
    var membres_array = this.state.membre;
    var my_id = this.state.id;
    var temp_revenu = 0;
    for(var i=0; i<membres_array.length;i++){
      if(!isNaN(membres_array[i])){
        var temp_metier = $('.people[data-id="'+membres_array[i]+'"] .metierPeople').html();
        if(temp_metier != undefined){
          var temp_argent = parseInt($('.metier[data-id="'+temp_metier+'"] .argent').html());
          if(!isNaN(temp_argent)){
            temp_revenu += temp_argent;
          }
        }
      }
    }
    this.setState({
      revenu: temp_revenu
    });
  }


  renderPeople(id){
    // DEBUT DE PARTIE SANS SAUVEGARDE
    if(!getsaved) {
      if(parseInt(id) > parseInt(maxId)){
        maxId = parseInt(id);
      }
  	  return(
  		    <People key={'people_id'+id} idPeople={id} />
  	  );
    // RECUPERATION SAUVEGARDE
    } else  {
      if(people_saved[id] != undefined){
        var nom         = people_saved[id].nom;
        var vitalite    = people_saved[id].vitalite;
        var maladie     = people_saved[id].maladie;
        var immunite    = people_saved[id].immunite;
        var metier      = people_saved[id].metier;
        var habitation  = people_saved[id].habitation;
        var sexe        = people_saved[id].sexe;
        var age         = people_saved[id].age;
        var education   = people_saved[id].education;
        var pregnancy   = people_saved[id].pregnancy;
        var humeur      = people_saved[id].humeur;
        var amour       = people_saved[id].amour;

        if(parseInt(id) > parseInt(maxId)){
          maxId = parseInt(id);
        }

    	  return(
    		    <People key={'people_id'+id} idPeople={id} nom={nom} vitalite={vitalite} maladie={maladie} immunite={immunite} metier={metier} habitation={habitation} sexe={sexe} age={age} education={education} pregnancy={pregnancy} humeur={humeur} amour={amour} />
    	  );
      // CAS PARTICULIER "BEBE"
      } else {
      	  return(
      		    <People key={'people_id'+id} idPeople={id} metier="bebe" age="0" />
      	  );
      }
    }
  }

  render() {

  	var people_liste = [];
    var mymembres;
    if(this.state.membre.length > 1){
      mymembres = this.state.membre.join();
    } else {
      mymembres = this.state.id;
    }

    var temp_class = "Famille ";

    for(var i=0; i<this.state.membre.length;i++){
      if(!isNaN(this.state.membre[i])){
        temp_class += "FamMem"+this.state.membre[i]+" ";
        if(getsaved) {
          people_liste.push(this.renderPeople(this.state.membre[i]));
        }
      }
    }
    if(!getsaved) {
     people_liste.push(this.renderPeople(this.state.id));
    }

	  return(
      <div className={temp_class} data-id={this.state.id}>
        { people_liste }
        <div className="membreFamille hidden">{mymembres}</div>
        <div className="revenuFamille hidden">{this.state.revenu}</div>
        <div className="depenseFamille hidden">{this.state.depense}</div>
        <div className="savingFamille hidden">{this.state.saving}</div>
        <div className="habitationFamille hidden">{this.state.habitation}</div>

        <div className="ajoutmembre"></div>
        <div className="suppmembre"></div>
        <button className="getRevenu hidden" onClick={this.getRevenu.bind(this, this.state.id)} ></button>
        <button className="changeHabitation hidden" onClick={this.setVarHab.bind(this, 'habitation', this.state.id)} ></button>
        <button className="ajoutM hidden" onClick={this.setVarAdd.bind(this, 'membre', this.state.id)} >AJOUT MEMBRE</button>
        <button className="supprM hidden" onClick={this.setVarSup.bind(this, 'membre', this.state.id)} >SUP MEMBRE</button>
      </div>
	  );
  }
}
