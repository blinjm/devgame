// ALL
var metiersGet;
var habitationsGet;
var current_month = 3;
var current_year = 1;
var current_saison = 'printemps';
var array_month = [1,2,3,4,5,6,7,8,9,10,11,12];
var maxId = 0;
var array_people = [];
var idFamille = 0;
var people_saved;
var famille_saved;
var game_saved;
var default_nbpeople = 30;
var getsaved = false;

var my_people_saved = localStorage.getItem("JSON_people");
if(my_people_saved != undefined && my_people_saved != null && my_people_saved != ""){
  people_saved = jQuery.parseJSON(my_people_saved);
  default_nbpeople = Object.keys(people_saved).length;
  // console.log(default_nbpeople);
  getsaved = true;
}

var my_famille_saved = localStorage.getItem("JSON_famille");
if(my_famille_saved != undefined && my_famille_saved != null && my_famille_saved != ""){
  famille_saved = jQuery.parseJSON(my_famille_saved);
}
var my_game_saved = localStorage.getItem("JSON_game");
if(my_game_saved != undefined && my_game_saved != null && my_game_saved != ""){
  game_saved = jQuery.parseJSON(my_game_saved);
  current_month = game_saved.current_month;
  current_year = game_saved.current_year;
  current_saison = game_saved.current_saison;
}

class All extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nbpeople: default_nbpeople
    };
  }

  /*renderPeople(id){
    if(parseInt(id) > parseInt(maxId)){
      maxId = parseInt(id);
    }
	  return(
		    <People key={'people_id'+id} idPeople={id} />
	  );
  }
  renderPeopleS(id){ // récupère les gens sauvegardés
    var nom = people_saved[id].nom;
    var vitalite = people_saved[id].vitalite;
    var maladie = people_saved[id].maladie;
    var immunite = people_saved[id].immunite;
    var metier = people_saved[id].metier;
    var habitation = people_saved[id].habitation;
    var sexe = people_saved[id].sexe;
    var age = people_saved[id].age;
    var education = people_saved[id].education;
    var pregnancy = people_saved[id].pregnancy;
    var humeur = people_saved[id].humeur;
    var amour = people_saved[id].amour;
    if(parseInt(id) > parseInt(maxId)){
      maxId = parseInt(id);
    }

	  return(
		    <People key={'people_id'+id} idPeople={id} nom={nom} vitalite={vitalite} maladie={maladie} immunite={immunite} metier={metier} habitation={habitation} sexe={sexe} age={age} education={education} pregnancy={pregnancy} humeur={humeur} amour={amour} />
	  );
  }*/

  renderFamilleS(id){

    var idF = id;
    var membre =  famille_saved[id].membre;
    var revenu = parseInt(famille_saved[id].revenu);
    var depense = parseInt(famille_saved[id].depense);
    var saving = parseInt(famille_saved[id].saving);
    var habitation =  famille_saved[id].habitation;

    if(membre.length == 0){
      revenu = 0;
      depense = 0;
      saving = 0;
    }

	  return(
      <Famille key={'famille_id'+idF} idFamille={idF} membre={membre} revenu={revenu} depense={depense} saving={saving} habitation={habitation} refFamille={this} />
	  );
  }

  renderFamille(id){
    idFamille++;
	  return(
      <Famille key={'famille_id'+idFamille} idFamille={idFamille} membre={id} refFamille={this} />
	  );
  }

  renderMetier(nom, edu, prod, argent, physique, ac1, ac2, ac3, ac4, debouche){
	  return(
		    <Metier key={'metier_id_'+nom} nom={nom} edu={edu} prod={prod} argent={argent} physique={physique} ac1={ac1} ac2={ac2} ac3={ac3} ac4={ac4} debouche={debouche} />
	  );
  }

  renderHabitation(nom, confort, taille, hygiene, achat, loc){
	  return(
		    <Habitation key={'habitation_id_'+nom} nom={nom} confort={confort} taille={taille} hygiene={hygiene} achat={achat} loc={loc} />
	  );
  }

  render() {
    var famille_liste = [];

    if(getsaved) {
      if(famille_saved != undefined && famille_saved != null && famille_saved != ""){
        for(var keyf in famille_saved){
          famille_liste.push(this.renderFamilleS(keyf));
        }
      }
    } else {
    	for(var i=1;i<=this.state.nbpeople;i++){
    		famille_liste.push(this.renderFamille(i));
    	}
    }

    var metier_liste = [];
    for(var j in metiersGet){
  		metier_liste.push(this.renderMetier(j, metiersGet[j].education, metiersGet[j].production, metiersGet[j].argent, metiersGet[j].physique, metiersGet[j].activite_saison1, metiersGet[j].activite_saison2, metiersGet[j].activite_saison3, metiersGet[j].activite_saison4, metiersGet[j].debouche));
    }

    var habitation_liste = [];
    for(var j in habitationsGet){
  		habitation_liste.push(this.renderHabitation(j, habitationsGet[j].confort, habitationsGet[j].taille, habitationsGet[j].hygiene, habitationsGet[j].achat, habitationsGet[j].loc));
    }

	  return(
  		<div className="wrapperContenu">
  			{ famille_liste }
  			{ metier_liste }
  			{ habitation_liste }
  		</div>
	  );
  }
}

var blockAll;

$.getJSON( "../js/data/metiers.json", function( data ) {
  metiersGet = data;

  $.getJSON( "../js/data/habitation.json", function( data2 ) {
    habitationsGet = data2;
    idFamille = 0;
    //blockAll.forceUpdate();
    blockAll = ReactDOM.render(
      <All />,
      document.getElementById('root')
    );
  });
});
