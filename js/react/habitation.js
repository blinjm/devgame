// METIER
class Habitation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
  		nom: this.props.nom,
      confort: this.props.confort,
      taille: this.props.taille,
      hygiene: this.props.hygiene,
      achat: this.props.achat,
      loc: this.props.loc
    };
  }

  render() {
	  return(
      <div className="habitation" data-nom={this.props.nom}>
        <div className="confort hidden">{this.props.confort}</div>
        <div className="taille hidden">{this.props.taille}</div>
        <div className="hygiene hidden">{this.props.hygiene}</div>
        <div className="achat hidden">{this.props.achat}</div>
        <div className="loc hidden">{this.props.loc}</div>
      </div>
	  );
  }
}
