// PEOPLE
var array_sexe = ['F', 'M'];
var default_age = 20;
var array_maxeduc = [0,1];
var defaultMetier = "paysan";
var defaultHabitation = "hutte";

class People extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.idPeople ,
  		nom: (this.props.nom)? this.props.nom:this.generateRandomName(),
  		vitalite: (this.props.vitalite)? this.props.vitalite:10,
      maladie: (this.props.maladie)? this.props.maladie:"", // lien vers une base de maladies -> niveau de contagion / niveau de gravité
      immunite: (this.props.immunite)? this.props.immunite:this.randomImmune(), // json d'immunités
      metier: (this.props.metier)? this.props.metier:defaultMetier, // json de métiers (id vers une base de métiers) avec niveau d'experience
      habitation: (this.props.habitation)? this.props.habitation:defaultHabitation, // json d'habitation -> niveau d'hygiene / confort / taille / id de l'habitation pour connaitre les personnes vivants ensemble
      /*argent: Math.floor(Math.random() * 45),*/
      sexe: (this.props.sexe)? this.props.sexe:array_sexe[Math.floor(Math.random() * array_sexe.length)],
      age: (this.props.age != undefined)? this.props.age:default_age,
      education: (this.props.education)? this.props.education:0,
      pregnancy: (this.props.pregnancy)? this.props.pregnancy:0,
      humeur: (this.props.humeur)? this.props.humeur:7,
      amour: (this.props.amour)? this.props.amour:-1,
      enfants: (this.props.enfants)? this.props.enfants:[],
      experience: (this.props.experience)? this.props.experience:0,
      maxeduc: (this.props.maxeduc)? this.props.maxeduc:array_maxeduc[Math.floor(Math.random() * array_maxeduc.length)], // random mais pas que (suivant les niveaux des parents)
      caractere1: (this.props.caractere1)? this.props.caractere1:"", // lien vers base de caracteres. caractere1 = caractere principal
      caractere2: (this.props.caractere2)? this.props.caractere2:""  // lien vers base de caracteres. caractere2 = caractere secondaire (pas pris en compte quand va à l'encontre du caractere1)
    };
	  var generateRandomName = this.generateRandomName.bind(this);
  }

  randomImmune(){
    var immune = Math.round(Math.random()*1);
    if(immune == 1){
      return '1';
    } else {
      return "";
    }
  }

  generateRandomName(){
    var consonnes  = ['b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z'];
    var voyelles   = ['a','e','i','u','y'];
    var nameTemp   = "";

    nameTemp += consonnes[Math.floor(Math.random() * consonnes.length)];
    nameTemp += voyelles[Math.floor(Math.random() * voyelles.length)];
    nameTemp += consonnes[Math.floor(Math.random() * consonnes.length)];
    nameTemp += voyelles[Math.floor(Math.random() * voyelles.length)];

    return nameTemp;
  }

  setVar(field, setting){
    this.setState({
      [field]: setting
    });
  }

  setVarA(field, id){
    var tempId = parseInt($('.people[data-id="'+id+'"] button.marry').html());
    if(tempId != -1){
      this.setState({
        [field]: tempId
      });
    }
  }

  setVarMetier(field, id){
    var tempId = ($('.people[data-id="'+id+'"] .changeMetier').html());
    console.log('Changement métier '+id+' -- change: '+this.state.metier+' pour '+tempId);
    if(tempId != -1){
      this.setState({
        [field]: tempId
      });
    }
  }

  getEnfant(id){
    var myenfant = $('.people[data-id="'+id+'"] .getEnfant').html();
    var array_enfant_deja = this.state.enfants;
    array_enfant_deja.push(myenfant);
    this.setState({
      enfants: array_enfant_deja
    });
  }

  render() {

    var newage = parseInt(this.state.age) + 1;

	  return(
  		<div className="people" data-id={this.state.id} data-metier={this.state.metier}>
        <div className="nomPeople hidden">{this.state.nom}</div>
        <div className="vitalitePeople hidden">{this.state.vitalite}</div>
        <div className="maladiePeople hidden">{this.state.maladie}</div>
        <div className="immunitePeople hidden">{this.state.immunite}</div>
        <div className="metierPeople hidden">{this.state.metier}</div>
        <div className="habitationPeople hidden">{this.state.habitation}</div>
        <div className="sexePeople hidden">{this.state.sexe}</div>
        <div className="agePeople hidden">{this.state.age}</div>
        <div className="educationPeople hidden">{this.state.education}</div>
        <div className="pregnancyPeople hidden">{this.state.pregnancy}</div>
        <div className="humeurPeople hidden">{this.state.humeur}</div>
        <div className="amourPeople hidden">{this.state.amour}</div>
        <div className="experiencePeople hidden">{this.state.experience}</div>

        <div className="enfantsPeople hidden"></div>

        <div className="maxeducPeople hidden">{this.state.maxeduc}</div>
        <div className="caractere1People hidden">{this.state.caractere1}</div>
        <div className="caractere2People hidden">{this.state.caractere2}</div>

        <button className="changeMetier hidden" onClick={this.setVarMetier.bind(this, 'metier', this.state.id)} ></button>
        <button className="marry hidden" onClick={this.setVarA.bind(this, 'amour', this.state.id)} >MARRIES</button>
        <button className="age hidden" onClick={this.setVar.bind(this, 'age', newage)} >AGE+</button>
        <button className="mort hidden" onClick={this.setVar.bind(this, 'vitalite', 0)} >mort</button>
        <button className="maladie hidden" onClick={this.setVar.bind(this, 'maladie', "m1")} >mymaladie</button>
        <button className="guerison hidden" onClick={this.setVar.bind(this, 'maladie', "")} >guérit</button>
        <button className="getEnfant hidden" onClick={this.getEnfant.bind(this, this.state.id)} ></button>
        <button className="getPregnant hidden" onClick={this.setVar.bind(this, 'pregnancy', 9)} ></button>
        <button className="getPregnant2 hidden" onClick={this.setVar.bind(this, 'pregnancy', (this.state.pregnancy - 1))} ></button>
        <button className="vitalite hidden" onClick={this.setVar.bind(this, 'vitalite', (this.state.vitalite - 1))} >mymaladie</button>
        <button className="xp hidden" onClick={this.setVar.bind(this, 'experience', (this.state.experience + 1))} >mymaladie</button>
  		</div>
	  );
  }
}
