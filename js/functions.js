/** MAKHEIA **/
// This function return a message in the browser console to show off
if (window.console) {
  var style;
  style = "background: #4daf26; color: #FFF; padding: 3px 5px; border-left: 5px solid #378718; margin-bottom: 3px;";
  var message = "This eLearning module has been developped by JMBLIN (https://webdevsan.com/).";
  console.log('%c'+message, style);
}

/** DEBUG **/
// Display console log with color
function debug_jmb(text, color){
  var style;
  style = "background: "+color+"; color: #FFF; padding: 3px 5px; margin-bottom: 3px;";
  var message = text;
  if (window.console) {
    console.log('%c'+message, style);
  }
}

/** RESIZE **/
// Display the overlay screen when the mobile is hold on paysage view
function resize () {
  if ($(window).width() > $(window).height()) {
    if($(window).width() < 768){
      $('#bascule-overlay').show();
    }
  } else {
    $('#bascule-overlay').hide();
  }
}

// Basic scrolling with nicescroll
function scrolling() {
  $('.scrollcheck').niceScroll({autohidemode: false,emulatetouch: true, grabcursorenabled:false});
}

/** APPEL DE PAGE **/
// url_page: page url you want to display
// body_class: the class you want for your body tag
// inmain: the dom part where the page will load
function gotopage(url_page, body_class, inmain){

  $('.transition').addClass('fade');
  $('body').attr('data-lang', language);

  // On attend que l'écran de transition se soit affiché
  setTimeout(function () {

    // Choix de la target (par défaut #scroller)
    var target = $( "#scroller" );
    if(inmain != undefined && inmain != "" && inmain != null) {
      target = $(''+inmain);
    }

    // Affichage de la page en ajax
    target.load( url_page, function() {
      if(body_class != undefined && body_class != ""){
        $('body').attr('class', '').addClass(body_class);
      }

      // on scroll en haut de page quand tout est chargé
      if(myScroll) {
        myScroll.scrollTo(0, 0, 0);
      }

      // on traduit la nouvelle page (language.js)
      trad(language);

      // on raraichit le scroll pour qu'il soit à la bonne taille à cause du nouveau texte
      setTimeout(function () {
        if(myScroll) {
          myScroll.refresh();
        }

        // On cache l'écran de transition
        $('.transition').addClass('fade_trans');
        setTimeout(function () {
          $('.transition').removeClass('fade fade_trans');
        }, 500);
      }, 501);

      checkscroll();
    });
  }, 500);
}

// Gestion des scroll en fonction des devices / navigateur
// Le comportement sur IE 11 est un peu spécial
function checkscroll(){

    var testie = isIE();
    $(".scrollcheck.content").each(function(){
      var ma_height = $(this).height();
      var content = ma_height;
      if($(this).children().length > 0){
        content = 0;
        $(this).children().each(function () {
          content += $(this).outerHeight(true);
        });
        // marge
        content -= 2;
      }
      if(ma_height >= content){
        $(this).getNiceScroll().hide();
      } else {
        $(this).getNiceScroll().show();
      }
    });

    $(".scroll").each(function(){
      var ma_height = $(this).height();
      var content = ma_height;
      if($(this).children().length > 0){
        content = 0;
        $(this).children().each(function () {
          content += $(this).outerHeight(true);
        });
        // marge
        content -= 2;
      }
      //debug_mak(ma_height+" -- "+content, "green");
      if(ma_height >= content){
        $(this).getNiceScroll().hide();
      } else {
        $(this).getNiceScroll().show();
      }
    });

    if(testie){
      $('body').addClass('menuie');
      $('.nicescroll-rails.nicescroll-rails-vr').each(function(){
        var moncursor = $(this).find('.nicescroll-cursors');
        if($('.nicescroll-rails').height() < (moncursor.height() + 10)){
          $(this).hide().addClass('removed');
        }
      });
      $('.nicescroll-rails.nicescroll-rails-hr').each(function(){
        $(this).hide().addClass('removed');
      });
    }
}

// Check if browser is IE
function isIE() {
  ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones*/
  var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

  return is_ie;
}

function saveData (){

    var JSON_people = {};
    $('.people').each(function(){
       var j_id = $(this).attr('data-id');
       var j_nom = $(this).find('.nomPeople').html();
       var j_vitalite = $(this).find('.vitalitePeople').html();
       var j_maladie = $(this).find('.maladiePeople').html();
       var j_immunite = $(this).find('.immunitePeople').html();
       var j_metier = $(this).find('.metierPeople').html();
       var j_habitation = $(this).find('.habitationPeople').html();
       var j_sexe = $(this).find('.sexePeople').html();
       var j_age = $(this).find('.agePeople').html();
       var j_education = $(this).find('.educationPeople').html();
       var j_pregnancy = $(this).find('.pregnancyPeople').html();
       var j_humeur = $(this).find('.humeurPeople').html();
       var j_amour = $(this).find('.amourPeople').html();
       var j_maxeduc = $(this).find('.maxeducPeople').html();
       var j_carac1 = $(this).find('.caractere1People').html();
       var j_carac2 = $(this).find('.caractere2People').html();
       var j_experience = $(this).find('.experiencePeople').html();

       JSON_people[""+j_id] = {"nom": j_nom, "vitalite": j_vitalite, "maladie": j_maladie, "immunite": j_immunite, "metier": j_metier, "habitation": j_habitation, "sexe": j_sexe, "age": j_age, "education": j_education, "pregnancy": j_pregnancy, "humeur": j_humeur, "amour": j_amour, "maxeduc": j_maxeduc, "carac1": j_carac1, "carac2": j_carac2, "experience": j_experience};

       if($(this).hasClass('peopledestroy')){
         $(this).remove();
       }
    });

    localStorage.setItem("JSON_people", JSON.stringify(JSON_people));

    var JSON_famille = {};
    $('.Famille').each(function(){
       var j_id = $(this).attr('data-id');
       //console.log(j_id);
       var j_membre = $(this).find('.membreFamille').html();
       var j_revenu = $(this).find('.revenuFamille').html();
       var j_depense = $(this).find('.depenseFamille').html();
       var j_saving = $(this).find('.savingFamille').html();
       var j_habitation = $(this).find('.habitationFamille').html();

       JSON_famille[""+j_id] = {"membre": j_membre, "revenu": j_revenu, "depense": j_depense, "saving": j_saving, "habitation": j_habitation};
    });
    localStorage.setItem("JSON_famille", JSON.stringify(JSON_famille));

    var JSON_game = {};
    JSON_game = {"current_month": current_month, "current_year": current_year, "current_saison": current_saison};
    localStorage.setItem("JSON_game", JSON.stringify(JSON_game));

    var JSON_prod = {};
    JSON_prod = ville_ressources;
    localStorage.setItem("JSON_prod", JSON.stringify(JSON_prod));

    getsaved = true;
    people_saved = JSON_people;
    default_nbpeople = Object.keys(JSON_people).length;
    console.log('NB people '+default_nbpeople);
    famille_saved = JSON_famille;
    game_saved = JSON_game;
    current_month = game_saved.current_month;
    current_year = game_saved.current_year;
    current_saison = game_saved.current_saison;
}

function compareValues(key, order='asc') {
  return function(a, b) {
    if(!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = (typeof a[key] === 'string') ?
      a[key].toUpperCase() : a[key];
    const varB = (typeof b[key] === 'string') ?
      b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return (
      (order == 'desc') ? (comparison * -1) : comparison
    );
  };
}
