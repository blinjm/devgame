/** GLOBAL VAR **/
var go        = false;
var debug     = 1;
var is_mobile = false;
var myScroll;
var donnes_jeux;
var standalone = window.navigator.standalone,
userAgent = window.navigator.userAgent.toLowerCase(),
safari = /safari/.test( userAgent ),
ios = /iphone|ipod|ipad/.test( userAgent );

/*** VERSION ***/
var version = '0A-00.00.01';

var maladie = {"m1": {"gravite":1, "contagion": 1}};
var ville_ressources = {"nourriture": 200, "minage": 0, "artisanat": 0, "or": 0, "science": 0, "religion": 0, "militaire": 0, "besoin": [{"key":"nourriture", "value":0, "metier": "paysan"},{"key":"minage", "value":5, "metier": "minage"},{"key":"artisanat", "value":0, "metier": "artisan"},{"key":"or", "value":0, "metier": "marchand"},{"key":"science", "value":0, "metier": "scientifique"},{"key":"religion", "value":0, "metier": "religieux"},{"key":"militaire", "value":0, "metier": "militaire"}]};
// nourriture -> à consommer 1 par mois et par personne
// minage -> permet l'artisanat
// artisanat -> améliore l'humeur (indispensable en fonction du type d'habitat)
// or -> permet d'attirer des gens
// science -> permet de découvrir des technologies
// religion -> permet de débloquer des pouvoirs divins
// militaire -> permet de gérer les crises militaires (révoltes et attaques)

var my_ressources = localStorage.getItem("JSON_prod");
if(my_ressources != undefined && my_ressources != null && my_ressources != ""){
  ville_ressources = jQuery.parseJSON(my_ressources);
}

jQuery(document).ready(function($){
  setInterval(function(){
    checkdebutmain();
  }, 500);
});

function checkdebutmain(){
  document.documentElement.setAttribute("data-browser", navigator.userAgent);

    if($('.version').length > 0){
      $('.version').html('v:'+version);
    }

  window.addEventListener("load",function() {
  	// Set a timeout...
  	setTimeout(function(){
  		// Hide the address bar!
  		window.scrollTo(0, 1);
  	}, 0);
  });


  $('.nextmonth .btn').off().on('click', function(){
    $(this).hide();
    if(array_month[current_month] < (array_month.length+1)){
      current_month++;
    } else {
      current_month = 1;
      current_year++;
      plusUnAn();
    }
    if(current_month == 3){
      current_saison = 'printemps';
    } else if(current_month == 6){
      current_saison = 'été';
    } else if(current_month == 9){
      current_saison = 'automne';
    } else if(current_month == 12){
      current_saison = 'hiver';
    }

    finDeMois();
    saveData();
    blockAll.forceUpdate();
    $(this).show();
    saveData();
  });
}

function plusUnAn(){

  /*** VIEILLISSEMENT ***/
  var tempNbPeople = maxId;

  for(var i = 0; i <= maxId; i++){
    // Si le type n'est pas mort
    if(parseInt($('.people[data-id="'+i+'"] .vitalitePeople').html()) > 0){
      var son_age = parseInt($('.people[data-id="'+i+'"] .agePeople').html());
      $('.people[data-id="'+i+'"] .age').trigger('click');
      if(son_age > 44){
        var percentage = son_age - 40;
        var check_death = Math.round( Math.random() * 100);
        if(percentage > check_death){
          mortPeople(i);
        }
      }
    }
  }
}

function mortPeople(id){
  $('.people[data-id="'+id+'"] .mort').trigger('click');
  // Suppression de la personne dans l'habitation
  $('.Famille.FamMem'+id+' .suppmembre').html(id);
  $('.Famille.FamMem'+id+' .supprM').trigger('click');
  debug_jmb("Personne "+id+" morte", 'black');
}

var temp_H = 0;
var temp_F = 0;
function finDeMois(){

  // MATING
  var tempNbPeople = maxId;

  for(var i = 1; i <= maxId; i++){
    // AJOUT D'EXPERIENCE
    $('.people[data-id="'+i+'"] .xp').trigger('click');

    // Si le type n'est pas mort
    if(parseInt($('.people[data-id="'+i+'"] .vitalitePeople').html()) > 0){
      var temph_sexe = ($('.people[data-id="'+i+'"] .sexePeople').html());
      var temph_amour = parseInt($('.people[data-id="'+i+'"] .amourPeople').html());
      var temph_age = parseInt($('.people[data-id="'+i+'"] .agePeople').html());

      if(temph_sexe == 'M' && temph_amour == -1 && temph_age > 14){
      // if(temph_sexe == 'M' && temph_amour == -1){
        temp_H = 0;
        temp_F = 0;

        // On parcourt la liste des femmes disponibles
        for(var j = 0; j <= maxId; j++){
          var tempf_sexe = ($('.people[data-id="'+j+'"] .sexePeople').html());
          var tempf_age = parseInt($('.people[data-id="'+j+'"] .agePeople').html());
          var tempf_amour = parseInt($('.people[data-id="'+j+'"] .amourPeople').html());
          if(tempf_sexe == 'F' && tempf_amour == -1 && tempf_age > 12){
          // if(tempf_sexe == 'F' && tempf_amour == -1){
            // SET AMOUR ENTRE LES DEUX
            temp_H = i;
            temp_F = j;

            // if(temp_H > 30 || temp_F > 30 ){
            //   console.log('NEW M');
            // }

            $('.people[data-id="'+i+'"] button.marry').html(temp_F);
            $('.people[data-id="'+i+'"] button.marry').trigger('click');
            $('.people[data-id="'+j+'"] button.marry').html(temp_H);
            $('.people[data-id="'+j+'"] button.marry').trigger('click');

            // AJUSTEMENT FAMILLE MEMBRE
            $('.Famille.FamMem'+j+' .suppmembre').html(j);
            $('.Famille.FamMem'+j+' .supprM').trigger('click');
            $('.Famille.FamMem'+i+' .ajoutmembre').html(j);
            $('.Famille.FamMem'+i+' .ajoutM').trigger('click');

            break;
          }
        }
      }
    }
  }
  // \ MATING

  // MALADIES //
  for(var i = 1; i <= maxId; i++){
    // Si le type n'est pas mort
    if(parseInt($('.people[data-id="'+i+'"] .vitalitePeople').html()) > 0){
      var temph_habitation = ($('.people[data-id="'+i+'"] .habitationPeople').html());
      var temph_maladie = ($('.people[data-id="'+i+'"] .maladiePeople').html());
      var temph_immune = ($('.people[data-id="'+i+'"] .immunitePeople').html());
      var temph_vitalite = ($('.people[data-id="'+i+'"] .vitalitePeople').html());
      var my_hygiene = parseInt($('.habitation[data-nom="'+temph_habitation+'"] .hygiene').html());
      // hygiene de 0 à 4
      var coeff_maladie = 4 - my_hygiene;
      // saison
      if(current_saison == 'hiver'){
        coeff_maladie+=2;
      }
      //vitalité
      if(temph_vitalite < 4){
        coeff_maladie+=2;
      }

      var randomscore = Math.floor(Math.random() * 200);
      // immunite
      if(temph_immune == "1"){
        randomscore = 200;
      }
      if(randomscore < coeff_maladie && temph_maladie.length == 0){
        $('.people[data-id="'+i+'"] .maladie').trigger('click');
        $('.people[data-id="'+i+'"] .vitalite').trigger('click');
        debug_jmb("Personne "+i+" malade", 'red');
      } else if (temph_maladie.length > 0){
        if(randomscore > (coeff_maladie + 100)) {
          $('.people[data-id="'+i+'"] .guerison').trigger('click');
          debug_jmb("Personne "+i+" guérie", 'red');
        } else if(randomscore > (coeff_maladie + 25)) {

        } else {
          $('.people[data-id="'+i+'"] .vitalite').trigger('click');
          if((temph_vitalite -1) <= 0){
            mortPeople(i);
          }
        }
      }
    }
  }
  //\ MALADIES //

  /*** EDUCATION ----------------------------------------------------------------------------***/

  // TRAVAIL //
  for(var t = 1; t <= maxId; t++){

    // Génération de la production
    if(parseInt($('.people[data-id="'+t+'"] .vitalitePeople').html()) > 0){
      var temp_metier = ($('.people[data-id="'+t+'"] .metierPeople').html());
      var temp_prod = ($('.metier[data-id="'+temp_metier+'"] .production').html());
      var temp_nbprod = 0;
      if(current_saison == 'printemps'){
        temp_nbprod = parseInt($('.metier[data-id="'+temp_metier+'"] .activite_saison1').html());
      } else if(current_saison == 'été'){
        temp_nbprod = parseInt($('.metier[data-id="'+temp_metier+'"] .activite_saison2').html());
      } else if(current_saison == 'automne'){
        temp_nbprod = parseInt($('.metier[data-id="'+temp_metier+'"] .activite_saison3').html());
      } else if(current_saison == 'hiver'){
        temp_nbprod = parseInt($('.metier[data-id="'+temp_metier+'"] .activite_saison4').html());
      }
      ville_ressources[temp_prod] += temp_nbprod;
    }

    // Changement de travail
    // - en fonction des besoins
    // vérification des besoins (0 = aucun besoin)
    var save_besoin = ville_ressources.besoin;
    save_besoin.sort(compareValues('value', 'desc'));
    //console.log(save_besoin);
    for(var bes in save_besoin){
      if(save_besoin[bes].value > 0){
        if(parseInt($('.people[data-id="'+t+'"] .vitalitePeople').html()) > 0 && parseInt($('.people[data-id="'+t+'"] .agePeople').html()) > 11){
          var tempb_metier = ($('.people[data-id="'+t+'"] .metierPeople').html());
          var tempb_prod = ($('.metier[data-id="'+tempb_metier+'"] .production').html());
          // changement de métier par besoin : seulement si on n'est pas déjà dans le domaine concerné
          // console.log(tempb_prod+' -- '+save_besoin[bes].key);
          if(tempb_prod != save_besoin[bes].key){
            var temp_debouche = ($('.metier[data-id="'+tempb_metier+'"] .debouche').html());
            var array_debouche = temp_debouche.split(',');
            var array_proddebouche = [];
            var array_proddebouche2 = [];
            var temp_newor = 0;
            var temp_education = 0;
            for(var debou = 0; debou < array_debouche.length; debou++){
              var temp_deb = ($('.metier[data-id="'+array_debouche[debou]+'"] .production').html());
              var temp_nor = parseInt($('.metier[data-id="'+array_debouche[debou]+'"] .argent').html());
              var temp_edu = parseInt($('.metier[data-id="'+array_debouche[debou]+'"] .education').html());
              if(temp_deb == save_besoin[bes].key){
                array_proddebouche.push(temp_deb);
                array_proddebouche2.push(array_debouche[debou]);
                temp_newor = temp_nor;
                temp_education = temp_edu;
              }
            }

            // var temp_education = parseInt($('.metier[data-id="'+tempb_metier+'"] .education').html());
            var temp_oldor = parseInt($('.metier[data-id="'+tempb_metier+'"] .argent').html());
            var temp_educationH = parseInt($('.people[data-id="'+t+'"] .educationPeople').html());

            // console.log(temp_education+' -- '+temp_educationH);
            // if(temp_education <= temp_educationH){
            //     debug_jmb('CHANGEMENT DE METIER - 0', 'black');
            // }
            console.log(array_proddebouche);
            console.log(tempb_prod);
            if(temp_education <= temp_educationH && array_proddebouche.indexOf(tempb_prod) > -1){
                debug_jmb('CHANGEMENT DE METIER - 1', 'black');
            }
            // Vérification si l'éducation est ok et si il y a une possibilité d'évolution et si l'argent est meilleur
            if(temp_education <= temp_educationH && array_proddebouche.indexOf(tempb_prod) > -1 && temp_newor > temp_oldor){
              debug_jmb('CHANGEMENT DE METIER - 2', 'black');
              $('.people[data-id="'+t+'"] .changeMetier').html(array_proddebouche2[0]).trigger('click');
              save_besoin[bes].value = parseInt(save_besoin[bes].value) - 1;
            }
          }
        }
      }
    }

    // - en fonction de l'experience
  }
  // Revenu pour les familles
  $('.Famille').each(function(){
    // AJOUTE LES REVENUS - SOUSTRAIT LES DEPENSES
    $(this).find('.getRevenu').trigger("click");
  });
  //\ TRAVAIL //

  /*** TAXES ------------------------------------------------------------------------------------***/

  /*** HABITATIONS ------------------------------------------------------------------------------***/
  /** - gestion amélioration habitation
      - gestion dévaluation habitation
      - gestion nouvelle habitation
      - ajustement de l'humeur en fonction du nombre d'habitant / taille
      - confort ?
  **/
  $('.Famille').each(function(){

    var temp_sav = parseInt($(this).find('.savingFamille').html());
    var temp_dep = parseInt($(this).find('.depenseFamille').html());
    var temp_rev = parseInt($(this).find('.revenuFamille').html());
    var temp_nom = $(this).find('.habitationFamille').html();

    if(temp_nom == "hutte"){
      var temp_achat = parseInt($('.habitation[data-nom="petite maison"] .achat').html());
      var temp_loc = parseInt($('.habitation[data-nom="petite maison"] .loc').html());
      // ARGENT SUFFISANT + REVENUS DANS LE VERT
      if(temp_achat < (temp_sav+10) && (temp_rev-temp_loc) > 0) {
        $(this).find('changeHabitation').html('petite maison').trigger('click');
      }
    }

  });

  /*** NAISSANCES ------------------------------------------------------------------------------***/
  for(var n = 1; n <= maxId; n++){

    if(parseInt($('.people[data-id="'+n+'"] .vitalitePeople').html()) > 0){
      var temp_sexe = ($('.people[data-id="'+n+'"] .sexePeople').html());
      var temp_amour = ($('.people[data-id="'+n+'"] .amourPeople').html());
      var temp_age = parseInt($('.people[data-id="'+n+'"] .agePeople').html());
      var temp_preg = parseInt($('.people[data-id="'+n+'"] .pregnancyPeople').html());
      var temp_vita = parseInt($('.people[data-id="'+n+'"] .vitalitePeople').html());

      if(temp_sexe == 'F' && temp_amour >= 0 && (temp_age > 13 && temp_age < 50) && temp_preg == 0){
        // IMPACT DE L'HABITATION SUR LA PROCHAINE NAISSANCE
        var temp_habitation = $('.Famille.FamMem'+n+' .habitationFamille').html();
        var temp_membre = $('.Famille.FamMem'+n+' .membreFamille').html();
        var temp_array_membre = temp_membre.split(',');
        var temp_taille = parseInt($('.habitation[data-nom="'+temp_habitation+'"] .taille').html());
        var assez_grand = (temp_taille*2) - temp_array_membre.length;
        if(assez_grand <= 0){
          assez_grand = Math.abs(assez_grand) * 20 + 10;
        } else {
          assez_grand = 0;
        }

        var random_preg = Math.round(Math.random() * (60+assez_grand)) - temp_vita;
        if(random_preg < 1){
          $('.people[data-id="'+n+'"] .getPregnant').trigger('click');
          debug_jmb(n+' est enceinte', 'green');
        }

      } else if(temp_preg > 0){
        $('.people[data-id="'+n+'"] .getPregnant2').trigger('click');
        if(temp_preg == 1){
          if(people_saved == undefined){
            people_saved = {};
          }
          default_nbpeople++;
          $('.wrapperContenu').append('<div class="people peopledestroy" data-id="'+default_nbpeople+'"><div class="agePeople">0</div><div class="metierPeople">bebe</div></div>');
          $('.people[data-id="'+n+'"] .getEnfant').html(default_nbpeople);
          $('.people[data-id="'+n+'"] .getEnfant').trigger('click');
          debug_jmb('naissance de '+default_nbpeople+' pour '+n, 'green');
          $('.Famille.FamMem'+n+' .ajoutmembre').html(default_nbpeople);
          $('.Famille.FamMem'+n+' .ajoutM').trigger('click');
        }
      }
    }

  }
  /*** MIGRATIONS ------------------------------------------------------------------------------***/
  /**
    - départ si humeur trop basse
    - arrivé si bonheur général
  **/

  /*** INCIDENTS ------------------------------------------------------------------------------***/
  /**
    - incident si humeur trop basse
  **/

  /*** CONSOMMATION ***/
  for(var c = 1; c <= maxId; c++){
    // CONSOMMATION DE NOURRITURE
    if(parseInt($('.people[data-id="'+c+'"] .vitalitePeople').html()) > 0){
      ville_ressources.nourriture = ville_ressources.nourriture - 1;
      if(ville_ressources.nourriture < 0){
        ville_ressources.nourriture = 0;
        $('.people[data-id="'+c+'"] .vitalite').trigger('click');
        var tempc_vitalite = ($('.people[data-id="'+c+'"] .vitalitePeople').html());
        if((tempc_vitalite - 1) <= 0){
          // $('.people[data-id="'+c+'"] .mort').trigger('click');
          mortPeople(c);
        }
      }
    }
    // \CONSOMMATION DE NOURRITURE
  }
}
